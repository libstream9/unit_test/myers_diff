#define BOOST_TEST_MODULE main
#include <boost/test/unit_test.hpp>

#include <stream9/myers_diff.hpp>

#include <string_view>

#include <stream9/strings/stream.hpp>

namespace testing {

namespace myers_diff = stream9::myers_diff;
namespace str = stream9::strings;

using namespace std::literals;

static boost::test_tools::per_element per_element {};

class event_recorder : public myers_diff::event_handler
{
public:
    event_recorder(std::string_view const a, std::string_view const b)
        : m_a { a }, m_b { b }
    {}

    void unchanged(uindex_t const x, uindex_t const y) override
    {
        using str::operator<<;

        std::string ev { "unchange: " };

        ev << "a[" << x << "] == "
           << "b[" << y << "] (" << m_a[x] << ")";

        m_events.push_back(std::move(ev));
    }

    void inserted(uindex_t const y) override
    {
        using str::operator<<;

        std::string ev { "insert:   " };

        ev << "b[" << y << "] (" << m_b[y] << ")";

        m_events.push_back(std::move(ev));
    }

    void deleted(uindex_t const x) override
    {
        using str::operator<<;

        std::string ev { "delete:   " };

        ev << "a[" << x << "] (" << m_a[x] << ")";

        m_events.push_back(std::move(ev));
    }

    auto begin() const
    {
        return m_events.begin();
    }

    auto end() const
    {
        return m_events.end();
    }

    auto size() const
    {
        return m_events.size();
    }

    auto empty() const
    {
        return m_events.empty();
    }

    friend std::ostream& operator<<(std::ostream& os, event_recorder const& r)
    {
        for (auto const& e: r) {
            os << e << "\n";
        }

        return os;
    }

private:
    std::string_view m_a;
    std::string_view m_b;
    std::vector<std::string> m_events;
};

BOOST_AUTO_TEST_CASE(empty_1_)
{
    auto const a = "";
    auto const b = "";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    BOOST_TEST(rec.empty());
}

BOOST_AUTO_TEST_CASE(empty_2_)
{
    auto const a = "";
    auto const b = "A";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "insert:   b[0] (A)", // insert A to a[cur], ++cur (1) -> "A"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(empty_3_)
{
    auto const a = "A";
    auto const b = "";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "delete:   a[0] (A)", // delete a[cur] -> ""
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(same_1_)
{
    auto const a = "ABC";
    auto const b = "ABC";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "unchange: a[0] == b[0] (A)", // ++cur
        "unchange: a[1] == b[1] (B)", // ++cur
        "unchange: a[2] == b[2] (C)", // ++cur
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(short_1_)
{
    auto const a = "A";
    auto const b = "B";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    // (insert, delete) or (delete, insert) can be interpret as replace
    auto const expected = { //cur = 0
        "insert:   b[0] (B)", // insert B to a[cur], ++cur (1) -> "BA"
        "delete:   a[0] (A)", // delete a[cur] -> "B"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(short_2_)
{
    auto const a = "AB";
    auto const b = "B";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "delete:   a[0] (A)", // delete a[cur] -> "B"
        "unchange: a[1] == b[0] (B)", // ++cur
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(short_3_)
{
    auto const a = "B";
    auto const b = "AB";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "insert:   b[0] (A)", // insert A to a[cur], ++cur -> "AB"
        "unchange: a[0] == b[1] (B)", // ++cur
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(short_4_)
{
    auto const a = "AB";
    auto const b = "CD";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    // (insert, delete) or (delete, insert) can be interpret as replace
    auto const expected = { // cur = 0
        "insert:   b[0] (C)", // insert C at a[cur], ++cur == 1 -> "CAB"
        "delete:   a[0] (A)", // delete a[cur] , "CB"
        "delete:   a[1] (B)", // delete a[cur], "C"
        "insert:   b[1] (D)", // insert D at a[cur], ++cur == 2 -> "CD"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(short_5_)
{
    auto const a = "ABC";
    auto const b = "BCA";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    // this could be inserpret as move a[0] to a[2]
    auto const expected = { // cur = 0
        "delete:   a[0] (A)", // delete a[cur] -> "BC"
        "unchange: a[1] == b[0] (B)", // ++cur == 1
        "unchange: a[2] == b[1] (C)", // ++cur == 2
        "insert:   b[2] (A)", // insert A to a[cur], ++cur == 3 -> "BCA"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(short_6_)
{
    auto const a = "ABC";
    auto const b = "BDCA";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    // this could be inserpret as move a[0] to a[3]
    auto const expected = { // cur = 0
        "delete:   a[0] (A)", // delete a[cur] -> "BC"
        "unchange: a[1] == b[0] (B)", // ++cur == 1
        "insert:   b[1] (D)", // insert D to a[cur], ++cur == 2 -> "BDC"
        "unchange: a[2] == b[2] (C)", // ++cur == 3
        "insert:   b[3] (A)", // insert A to a[cur], ++cur == 4 -> "BDCA"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(case_1_)
{
    auto const a = "ABCABBA";
    auto const b = "CBABAC";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "delete:   a[0] (A)", // delete a[cur] -> "BCABBA"
        "delete:   a[1] (B)", // delete a[cur] -> "CABBA"
        "unchange: a[2] == b[0] (C)", // ++cur (1)
        "insert:   b[1] (B)", // insert B to a[cur], ++cur (2) -> "CBABBA"
        "unchange: a[3] == b[2] (A)", // ++cur (3)
        "unchange: a[4] == b[3] (B)", // ++cur (4)
        "delete:   a[5] (B)", // delete a[cur] -> "CBABA"
        "unchange: a[6] == b[4] (A)", // ++cur (5)
        "insert:   b[5] (C)", // insert C to a[cur], ++cur (6) -> "BCABAC"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(case_2_)
{
    auto const a = "1234567890ABCDEFGH";
    auto const b = "0ABCDEFGH123456789";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "insert:   b[0] (0)", // insert 0 to a[cur], ++cur == 1, a == "01234567890ABCDEFGH"
        "insert:   b[1] (A)", // insert A to a[cur], ++cur == 2, a == "0A1234567890ABCDEFGH"
        "insert:   b[2] (B)", // insert B to a[cur], ++cur == 3, a == "0AB1234567890ABCDEFGH"
        "insert:   b[3] (C)", // insert C to a[cur], ++cur == 4, a == "0ABC1234567890ABCDEFGH"
        "insert:   b[4] (D)", // insert D to a[cur], ++cur == 5, a == "0ABCD1234567890ABCDEFGH"
        "insert:   b[5] (E)", // insert E to a[cur], ++cur == 6, a == "0ABCDE1234567890ABCDEFGH"
        "insert:   b[6] (F)", // insert F to a[cur], ++cur == 7, a == "0ABCDEF1234567890ABCDEFGH"
        "insert:   b[7] (G)", // insert G to a[cur], ++cur == 8, a == "0ABCDEFG1234567890ABCDEFGH"
        "insert:   b[8] (H)", // insert H to a[cur], ++cur == 9, a == "0ABCDEFGH1234567890ABCDEFGH"
        "unchange: a[0] == b[9] (1)", // ++cur == 10
        "unchange: a[1] == b[10] (2)", // ++cur == 11
        "unchange: a[2] == b[11] (3)", // ++cur == 12
        "unchange: a[3] == b[12] (4)", // ++cur == 13
        "unchange: a[4] == b[13] (5)", // ++cur == 14
        "unchange: a[5] == b[14] (6)", // ++cur == 15
        "unchange: a[6] == b[15] (7)", // ++cur == 16
        "unchange: a[7] == b[16] (8)", // ++cur == 17
        "unchange: a[8] == b[17] (9)", // ++cur == 18
        "delete:   a[9] (0)", // delete a[cur], a == "0ABCDEFGH123456789ABCDEFGH"
        "delete:   a[10] (A)", // delete a[cur], a == "0ABCDEFGH123456789BCDEFGH"
        "delete:   a[11] (B)", // delete a[cur], a == "0ABCDEFGH123456789CDEFGH"
        "delete:   a[12] (C)", // delete a[cur], a == "0ABCDEFGH123456789DEFGH"
        "delete:   a[13] (D)", // delete a[cur], a == "0ABCDEFGH123456789EFGH"
        "delete:   a[14] (E)", // delete a[cur], a == "0ABCDEFGH123456789FGH"
        "delete:   a[15] (F)", // delete a[cur], a == "0ABCDEFGH123456789GH"
        "delete:   a[16] (G)", // delete a[cur], a == "0ABCDEFGH123456789H"
        "delete:   a[17] (H)", // delete a[cur], a == "0ABCDEFGH123456789"
    };

    BOOST_TEST(rec == expected, per_element);
}

BOOST_AUTO_TEST_CASE(case_3_)
{
    auto const a = "A{BC_D_E}_1{2_3_4}";
    auto const b = "1{2_3_4}_A{BC_D_E}";

    event_recorder rec { a, b };
    myers_diff::diff(a, b, rec);

    auto const expected = { // cur = 0
        "insert:   b[0] (1)", // insert 1 to a[cur], ++cur == 1, a == "1A{BC_D_E}_1{2_3_4}"
        "delete:   a[0] (A)", // delete a[cur], a == "1{BC_D_E}_1{2_3_4}"
        "unchange: a[1] == b[1] ({)", // ++cur == 2
        "delete:   a[2] (B)", // delete a[cur], a == "1{C_D_E}_1{2_3_4}"
        "delete:   a[3] (C)", // delete a[cur], a == "1{_D_E}_1{2_3_4}"
        "insert:   b[2] (2)", // insert 2 to a[cur], ++cur == 3, a == "1{2_D_E}_1{2_3_4}"
        "unchange: a[4] == b[3] (_)", // ++cur == 4
        "insert:   b[4] (3)", // insert 3 to a[cur], ++cur == 5, a == "1{2_3D_E}_1{2_3_4}"
        "delete:   a[5] (D)", // delete a[cur], a == "1{2_3_E}_1{2_3_4}"
        "unchange: a[6] == b[5] (_)", // ++cur == 6
        "delete:   a[7] (E)", // delete a[cur], a == "1{2_3_}_1{2_3_4}"
        "insert:   b[6] (4)", // insert 4 to a[cur], ++cur == 7, a == "1{2_3_4}_1{2_3_4}"
        "unchange: a[8] == b[7] (})", // ++cur == 8
        "unchange: a[9] == b[8] (_)", // ++cur == 9
        "delete:   a[10] (1)", // delete a[cur], a == "1{2_3_4}_{2_3_4}"
        "insert:   b[9] (A)", // insert A to a[cur], ++cur == 10, a == "1{2_3_4}_A{2_3_4}"
        "unchange: a[11] == b[10] ({)", // ++cur == 11
        "insert:   b[11] (B)", // insert B to a[cur], ++cur == 12, a == "1{2_3_4}_A{B2_3_4}"
        "delete:   a[12] (2)", // delete a[cur], a == "1{2_3_4}_A{B_3_4}"
        "insert:   b[12] (C)", // insert C to a[cur], ++cur == 13, a == "1{2_3_4}_A{BC_3_4}"
        "unchange: a[13] == b[13] (_)", // ++cur == 14
        "delete:   a[14] (3)", // delete a[cur], a == "1{2_3_4}_A{BC__4}"
        "insert:   b[14] (D)", // insert D to a[cur], ++cur == 15, a == "1{2_3_4}_A{BC_D_4}"
        "unchange: a[15] == b[15] (_)", // ++cur == 16
        "delete:   a[16] (4)", // delete a[cur], a == "1{2_3_4}_A{BC_D_}"
        "insert:   b[16] (E)", // insert E to a[cur], ++cur == 17, a == "1{2_3_4}_A{BC_D_E}"
        "unchange: a[17] == b[17] (})", // ++cur == 18
    };

    BOOST_TEST(rec == expected, per_element);
}

} // namespace testing
